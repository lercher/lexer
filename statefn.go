package lexer

// StateFn is a function to provide state to a Lexer
type StateFn func(*Lexer) StateFn
