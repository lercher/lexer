package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"text/template"
)

// see https://github.com/nhoizey/vscode-gremlins/blob/master/package.json

const url = `https://raw.githubusercontent.com/nhoizey/vscode-gremlins/master/package.json`

var t = template.Must(template.New("").Parse(`// This code is generated by "go generate". DO NOT EDIT.

package lexer

var defaultGremlins = map[rune]string{
{{range $k, $v := .Default}}{{if eq "error" .Level}}    '\u{{$k}}': {{printf "%q" $v.Description}},
{{end}}{{end}}}`))

func main() {
	log.Println("get", url, "...")
	res, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	if res == nil {
		log.Fatal("response is nil")
	}
	if res.StatusCode != http.StatusOK {
		log.Fatal(res.StatusCode)
	}
	if res.Body == nil {
		log.Fatal("response body is nil")
	}
	body, err := io.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}
	res.Body.Close()

	err = os.WriteFile(filepath.Join("gremlins", "gremlins.json"), body, 0644)
	if err != nil {
		log.Fatal(err)
	}

	dec := json.NewDecoder(readAfterGremlinsCharacters(string(body)))

	var grems ch
	err = dec.Decode(&grems)
	if err != nil {
		log.Fatalln(err)
	}

	if len(os.Args) < 2 {
		log.Fatalln("missing output file command line parameter")
	}

	f, err := os.Create(os.Args[1]) // panics if called w/o arg
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()

	err = t.Execute(f, grems)
	if err != nil {
		log.Fatalln(err)
	}
}

func readAfterGremlinsCharacters(gremlinsJSON string) io.Reader {
	const x = `"gremlins.characters":`
	idx := strings.Index(gremlinsJSON, x)
	if idx < 0 {
		panic(x + " not found in resource")
	}
	return strings.NewReader(gremlinsJSON[idx+1+len(x):])
}

type ch struct {
	Default map[string]dl `json:"default,omitempty"`
}

type dl struct {
	Description string `json:"description,omitempty"`
	Level       string `json:"level,omitempty"`
}
