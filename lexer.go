package lexer

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"unicode/utf8"
)

const (
	// EOF is returned by Next and Peek if the Lexer is at EOF
	EOF rune = -1
	// LexerError is the default ErrorTokenType of a new Lexer
	LexerError int = -9
)

// Lexer is splits up an input string into tokens
// based on a state machine formed by a set of
// corresponding StateFn functions
type Lexer struct {
	ErrorTokenType int             // ErrorTokenType is used for the Token emitted on calling Errorf. It defaults to LexerError.
	SourceFile     string          // SourceFile is used only for error reporting
	Ctx            context.Context // Ctx is for cancelation of scanning and to pass around arbitrary values like symbol tables accessible by StateFn function of this Lexer
	DangerousRunes map[rune]string // DangerousRunes lead to a panic if read by Next() and dependants
	input          string
	start          int
	pos            int
	state          StateFn
	nexttokens     []Token
	rewindSizes    []int
	lasterror      string
	lastemit       string
	data           any
}

// New creates a returns a lexer ready to parse the given source encoded as utf8.
// The sourceFile is only for error reporting, there is no file operation.
//
// Note: The caller needs to ensure that "\r"s are removed if lexing tests
// for line-breaks only against "\n".
//
// Lexing panics if a rune in DangerousRunes is read by Next or dependants.
// To customize clear/nil, extend or delete DangerousRunes after New.
func New(
	sourceFile string,
	source string,
	start StateFn,
) *Lexer {
	return &Lexer{
		ErrorTokenType: LexerError,
		SourceFile:     sourceFile,
		Ctx:            context.Background(),
		DangerousRunes: copyGremlins(), // copy to not expose our generated default map
		input:          source,
		state:          start,
		start:          0,
		pos:            0,
		rewindSizes:    make([]int, 0, 2),
	}
}

//go:generate go run ./gremlins defaultgremlins.go

func copyGremlins() map[rune]string {
	cpy := make(map[rune]string, len(defaultGremlins))
	for k, v := range defaultGremlins {
		cpy[k] = v
	}
	return cpy
}

// NextToken returns the next token from the input.
func (l *Lexer) NextToken() (Token, bool) {
	for {
		switch {
		case l.Ctx.Err() != nil:
			l.state = l.Errorf("%v", l.Ctx.Err())

		case len(l.nexttokens) > 0:
			t := l.nexttokens[0]
			l.nexttokens = l.nexttokens[1:]
			return t, true

		case l.state != nil:
			l.state = l.state(l)

		default:
			// no more tokens and no more state
			return Token{}, false
		}
	}
}

// Current returns the value being being analyzed at this moment.
// This is usefull for Errorf.
func (l *Lexer) Current() string {
	return l.input[l.start:l.pos]
}

// EmitTrimmed will receive a token type and push a new token with the current analyzed
// value trimmed into the tokens channel.
// If the text is empty after trimming it is Ignored and not Emitted.
func (l *Lexer) EmitTrimmed(tt int) {
	txt := l.Current()
	txt = strings.TrimSpace(txt)
	if txt == "" {
		l.Ignore()
		return
	}
	l.EmitText(tt, txt)
}

// Emit will receive a token type and push a new token with the current analyzed
// value into the tokens channel.
func (l *Lexer) Emit(tt int) {
	l.EmitText(tt, l.Current())
}

// EmitText Ignore-s the Current text and instead Emit-s the
// given string.
func (l *Lexer) EmitText(tt int, txt string) {
	l.lastemit = txt
	tok := Token{
		Type:  tt,
		Value: txt,
	}
	l.nexttokens = append(l.nexttokens, tok)
	l.start = l.pos
	l.rewindSizes = l.rewindSizes[:0]
}

// Errorf returns an error token and terminates the scan
// by passing back a nil pointer that will be the next
// state, terminating l.run.
func (l *Lexer) Errorf(format string, args ...any) StateFn {
	l.lasterror = fmt.Sprintf(format, args...)
	line, col, _, tx, _ := l.Position()
	var err error
	if l.SourceFile != "" {
		err = fmt.Errorf("[%s:%d:%d@%q] %s", l.SourceFile, line, col, tx, l.lasterror)
	} else {
		err = fmt.Errorf("[%d:%d@%q] %s", line, col, tx, l.lasterror)
	}
	tok := Token{
		Type:  l.ErrorTokenType,
		Value: err.Error(),
	}
	l.nexttokens = append(l.nexttokens, tok)
	return nil
}

// Next pulls the next rune from the Lexer, stacks and returns it, moving the position
// forward in the source. It returns and stacks EOF if no more runes are available.
// If a DangerousRunes is detected Next Backups this rune and then panics.
func (l *Lexer) Next() rune {
	str := l.PeekAll()
	r, size := EOF, 0
	if len(str) != 0 {
		r, size = utf8.DecodeRuneInString(str)
	}
	l.pos += size
	l.rewindSizes = append(l.rewindSizes, size)

	if s, ok := l.DangerousRunes[r]; ok {
		l.Backup()
		panic(fmt.Errorf("dangerous rune %q at byte pos0 %v", s, l.pos))
	}
	return r
}

// Ignore clears the rewind stack and then sets the current beginning position
// to the current position in the source which effectively ignores the section
// of the source being analyzed.
func (l *Lexer) Ignore() {
	l.rewindSizes = l.rewindSizes[:0]
	l.start = l.pos
}

// Backup will take the last rune read (if any) and rewind back. Backup can
// occur more than once per call to Next but you can never rewind past the
// last point a token was emitted.
func (l *Lexer) Backup() {
	length := len(l.rewindSizes)
	if length == 0 {
		return
	}

	// pop size from l.rewindlen stack
	size := l.rewindSizes[length-1]
	l.rewindSizes = l.rewindSizes[:length-1]

	l.pos -= size
	if l.pos < l.start {
		l.pos = l.start
	}
}

// Peek performs a Next operation immediately followed by a Rewind returning the
// peeked rune.
func (l *Lexer) Peek() rune {
	r := l.Next()
	l.Backup()
	return r
}

// PeekAll returns the full remaining input
func (l *Lexer) PeekAll() string {
	return l.input[l.pos:]
}

// AcceptPrefix is true if the rest of the input starts
// with the given prefix, the prefix is consumed and can be Backup()-ed.
func (l *Lexer) AcceptPrefix(prefix string) bool {
	if strings.HasPrefix(l.PeekAll(), prefix) {
		l.rewindSizes = append(l.rewindSizes, len(prefix))
		l.pos += len(prefix)
		return true
	}
	return false
}

// AcceptPrefixFold is true if the rest of the input starts case-insensitvely
// with the given prefix, the prefix is consumed and can be Backup()-ed.
func (l *Lexer) AcceptPrefixFold(prefix string) bool {
	if hasPrefixFold(l.PeekAll(), prefix) {
		l.rewindSizes = append(l.rewindSizes, len(prefix))
		l.pos += len(prefix)
		return true
	}
	return false
}

// hasPrefixFold tests whether the string s begins with prefix case-insensitive.
func hasPrefixFold(s, prefix string) bool {
	return len(s) >= len(prefix) && strings.EqualFold(s[0:len(prefix)], prefix)
}

// EOF is true if all runes are consumed.
func (l *Lexer) EOF() bool {
	return l.pos >= len(l.input)
}

// AcceptFunc consumes the next rune if any predicate is true.
// AcceptFunc plays nicely with rune-set's Contains Predicate like
// e.g. this check for alpha-numerics:
// l.AcceptRunFunc(runes.In(unicode.Letter).Contains, runes.In(unicode.Digit).Contains)
func (l *Lexer) AcceptFunc(isOK ...func(r rune) bool) bool {
	r := l.Next()
	for _, fn := range isOK {
		if fn(r) {
			return true
		}
	}
	l.Backup()
	return false
}

// Accept consumes the next rune if it's from the valid set.
func (l *Lexer) Accept(validRunes string) bool {
	if strings.IndexRune(validRunes, l.Next()) >= 0 {
		return true
	}
	l.Backup()
	return false
}

// AcceptRunFunc consumes a run of runes if any predicate is true.
// The run may have length 0. The count of accepted runes is returned.
// See AcceptFunc for more documentation.
func (l *Lexer) AcceptRunFunc(isOK ...func(r rune) bool) (count int) {
	for l.AcceptFunc(isOK...) {
		count++
	}
	return count
}

// AcceptRun consumes a run of runes from the valid set.
// The run may have length 0. The count of accepted runes is returned.
func (l *Lexer) AcceptRun(validRunes string) (count int) {
	for l.Accept(validRunes) {
		count++
	}
	return count
}

// AcceptUntilFunc consumes a run of any runes until any isEnd predicate is true.
// The count of accepted runes is returned and it may be 0.
// The finishing rune is not accepted. If it needs to be, call
// Next once after AcceptUntilFunc returns.
// EOF always ends AcceptUntilFunc before any isEnd is called.
func (l *Lexer) AcceptUntilFunc(isEnd ...func(r rune) bool) (count int) {
	return l.AcceptRunFunc(func(r rune) bool {
		if r == EOF {
			return false // end AcceptRunFunc here
		}
		for _, fn := range isEnd {
			if fn(r) {
				return false // end AcceptRunFunc here
			}
		}
		return true // continue AcceptRunFunc
	})
}

// AcceptUntil consumes a run of any runes until a rune appears in endRunes.
// The count of accepted runes is returned.
// The finishing rune is not accepted. If it needs to be, call
// Next once after AcceptUntil returns.
// EOF always ends AcceptUntil, even if it is not contained in endRunes.
func (l *Lexer) AcceptUntil(endRunes string) (count int) {
	fn1 := func() bool {
		r := l.Next()
		return r != EOF && strings.IndexRune(endRunes, r) < 0
	}
	for fn1() {
		count++
	}
	l.Backup()
	return count
}

// Position returns 1-based line number and column number of the current position,
// the byte-index of the whole string, the current line marked with the
// current or last lexed text and the last emitted error string as err
func (l *Lexer) Position() (linenr1, colnr1, pos0 int, context string, err error) {
	before := l.input[:l.pos]

	nls := strings.Count(before, "\n")
	if nls > 0 {
		idx := strings.LastIndexByte(before, '\n')
		before = before[idx+1:]
	}

	countRunes := utf8.RuneCountInString(before)

	idxnl := strings.IndexByte(l.PeekAll(), '\n') + l.pos
	if idxnl < l.pos {
		idxnl = len(l.input)
	}
	//log.Println(l.pos, idxnl)

	cur := l.Current()
	if l.Current() == "" {
		cur = l.lastemit
	}
	context = fmt.Sprint(before, "^", cur, "^", l.input[l.pos:idxnl])

	if l.lasterror != "" {
		err = errors.New(l.lasterror)
	}

	return nls + 1, countRunes + 1, l.pos, context, err
}

type unwraper interface {
	lexer() *Lexer
}

func (l *Lexer) lexer() *Lexer {
	return l
}

// GetData reads the data associated with lexer l.
// It panics, if types don't match.
func GetData[T any](l *Lexer) *T {
	if l == nil {
		return nil
	}
	return l.data.(*T)
}

// SetData associates any data d with the lexer l.
func SetData[T any](l *Lexer, d *T) {
	l.data = d
}
