# Lexer [![Go Reference](https://pkg.go.dev/badge/gitlab.com/lercher/lexer.svg)](https://pkg.go.dev/gitlab.com/lercher/lexer)

Lexer inspired by Rob Pike's talk 2011 on lexing <https://go.dev/talks/2011/lex.slide#42>
and by <https://github.com/bbuck/go-lexer/blob/master/lexer.go>

The code is provided under the MIT License, and the package (without the tests)
uses only the Go standard library. The yacc adapter needs generics,
so it won't run on older Go versions as is. However, if there's
no yacc or `Data` there's no need for generics. A fork might remove these tools
requirement if need arises.

## State of Lexer, StateFn and Token

- The _core_ lexer framework (see API section) works as intended
  and is no more subject to changes
- I'm currently fiddling with its API, i.e. I consider adding more helper methods
  and I'm not yet positive on the names and signatures of the "Accept*" methods
- `Lexer.Ctx` may remain or may be removed in v1
- `Token` may or may not have positional information in v1

Ignored text, like commments, is very unlikely to get an extra token stream in `Lexer`
b/c there is a canonical workaround:
An implementor can e.g. `Emit` comment-like tokens as needed via the normal
token stream and then wrap `NextToken` to split this token stream into two.

## State of the yacc Adapter

- The yacc adapter is WIP, b/c I need to find a good way to "exfiltrate"
  yacc's `$$` value of the start NT-symbol and to pass parser state information
  over to the lexer. E.g. for symbol table management

## Differences to the 2011 Design

- lexer and lexer helper functions are public and therefor start with a capital letter
- emitted errors automatically include line and column prefix
- `item` is called `Token` just b/c we guess it's more common among "parser folks"
  to call terminal symbols token rather than item
- this implementation goes the "Traditional lexer API" way only
  and it doesn't use the Go antipattern "channel with capacity instead of a queue"
- `Backup()` can undo more than one `Next()` call
- the API provides a couple of extra lexer and lexer helper functions, based
  on the most basic `Next`, `Backup` and `Peek`.

## Lexer API

`go doc lexer.lexer` shortened, we denote the _core_ API with a `*`, meaning it's
very unlikely that we change sth until v1. For v1 we intend to not change the API
any more. Nor its behavior, with one exception: the contents of `DangerousRunes`
might be changed for security reasons. If you absolutely need stability
in `DangerousRunes`, clear it after `New` and deposit your own fixed list there.

```go
type Lexer struct {
*       ErrorTokenType int
*       SourceFile     string
        Ctx            context.Context 
        DangerousRunes map[rune]string
}

* func New(sourceFile string, source string, start StateFn) *Lexer
  func (l *Lexer) Accept(validRunes string) bool
  func (l *Lexer) AcceptFunc(isOK ...func(r rune) bool) bool
  func (l *Lexer) AcceptPrefix(prefix string) bool
  func (l *Lexer) AcceptPrefixFold(prefix string) bool
  func (l *Lexer) AcceptRun(validRunes string) (count int)
  func (l *Lexer) AcceptRunFunc(isOK ...func(r rune) bool) (count int)
  func (l *Lexer) AcceptUntil(endRunes string) (count int)
  func (l *Lexer) AcceptUntilFunc(isEnd ...func(r rune) bool) (count int)
* func (l *Lexer) Backup()
* func (l *Lexer) Current() string
* func (l *Lexer) EOF() bool
* func (l *Lexer) Emit(tt int)
* func (l *Lexer) EmitText(tt int, txt string)
* func (l *Lexer) EmitTrimmed(tt int)
* func (l *Lexer) Errorf(format string, args ...any) StateFn
* func (l *Lexer) Ignore()
* func (l *Lexer) Next() rune
* func (l *Lexer) NextToken() (Token, bool)
* func (l *Lexer) Peek() rune
  func (l *Lexer) PeekAll() string
* func (l *Lexer) Position() (linenr1, colnr1, pos0 int, context string, err error)
```

Most methods are for `StateFn` implementations providing
concrete lexers.

To construct a lexer instance, call `New` with the
initial state of the new lexer. Then repeatedly call `NextToken()`
to stream the lexed `Token`s until the returned bool is `false`.

### Data on Lexer and YaccAdapter

_Note:_ Cooperation with yacc is described in its own chapter below,
yacc is somewhat "nasty" b/c it is old from today's point of viw
and "C", but it is well-known and stable. This section is here to get
rid of global variables, non-reentrace and excessive type casting.

To associate arbitrary data with a lexer, i.e. a pointer to a struct,
for communication of the environment, normally a parser, with the `StateFn`
functions, we provide these functions, where `LX` indicates typesafe
use when a `Lexer` is available like inside `StateFn`s and during setup
and tear-down. `YA` stands for `YaccAdapter` and is useful during semantic
actions to communicate between parsing state and lexer state.

_Important Note:_ anything called "data" here points to the same instance stored
with the `Lexer` item itself or the wraped `Lexer` of a `YaccAdapter`.

`SetResult` and `Result` on a `YaccAdapter` is handy when exfiltrating an
`yySymType` (aka `%union`) item of a yacc parser stack containing `RVal`s
during a semantic action.
It operates on yacc's `%union` type b/c the adapter already knows this type
via a Go type parameter, so there is no duplication.
This also allows to access more than one piece of data,
like when a production returns `$<val1>$ = sth; $<val2>$ = sthElse; ...`.

If you want to pass one or multiple `$n` macros to the `Lexer` this can also be
achieved via `Data` but then you have to define fields on `T` to duplicate the
data you already have in `%union` and `StateFn` functions also "see" these fields
intended for result.

```go
LX   func GetData[T any](l *Lexer) *T
LX   func SetData[T any](l *Lexer, d *T)

YA   func Data[T any](yylex any) *T
YA   func WithData[YYSymType, T any](data *T, ya *YaccAdapter[YYSymType]) *YaccAdapter[YYSymType]

YA   func SetResult[YYSymType any](yylex any, yyDollar []YYSymType, idx int)
```

These helpers save some nasty type casts and hopefully avoid errors due to
value/pointer/pointer-to-pointer mismatches coming from `any`.
See our yaccscenario subfolder for a comprehensive example of how this works
together. The section "The yacc Scenario Shows" below has more prose on it.

## Constructing `StateFn` Functions

`StateFn` functions model multiple modes a lexer can be in.
E.g. a computer language lexer normally starts lexing keywords and punctuation
of the language. When it notices that a string literal or a comment starts
it need to change its lexing mode or "state" to scanning the string literal
contents where you don't want to emit tokens when a keyword shows up.

This design keeps the lexer state by `StateFn` functions that return
other `StateFn` functions when called or `nil` for lexing to stop.
We recommend to have a look at the slides of the 2011 design at
<https://go.dev/talks/2011/lex.slide> first. There is also a
[51min talk on YouTube](https://www.youtube.com/watch?v=HxaD_trXwRE)
where the design was presented.
Then have a look at the `*_test.go` files for some examples: look out
for functions called `lexSomething(l *Lexer) StateFn`.

Their birds-eye structure is usually:

- `Peek|Next|Accept*` runes inside a big switch and let `Current` grow by this
- Occasionally `Backup` from a non-fitting rune to shrink `Current` a bit
- Occasionally `Ignore` some runes, like whitespace or comments, emptying `Current`
- `Emit*` some token type, which associates `Current`, the previously accepted
  text, with the emitted `Token` and clears `Current`.
- On errors `return l.Errorf(...)` to signal a lexer-error, like an unterminated
  string literal or a malformed number literal
- Finally return the own or a different `StateFn` to keep or change the current lexing mode
- On `EOF` return `nil` to signal the end of the token stream

## Example Lexer

See [lexer_test.go](https://gitlab.com/lercher/lexer/-/blob/main/lexer_test.go?ref_type=heads#L20)
where this forms the example lexer:

```go
const (
  ErrorToken = iota
  TextToken
  StringToken
  IntToken
)

func lexComment(l *Lexer) StateFn {}
func lexText(l *Lexer) StateFn {}
func lexInt(l *Lexer) StateFn {}
func lexString(l *Lexer) StateFn {}
```

And this retrieves the stream of tokens:

```go
func TestLexer(t *testing.T) {
  l := New(input, ErrorToken, lexText)
  for {
    tok, ok := l.NextToken()
    if !ok {
      t.Log("done")
      break
    }
    t.Logf("%#v", tok)
  }
}
```

## Dangerous Runes

The map `DangerousRunes` leads to a panic by `Next()` and dependant
helper functions when a rune read by `Next()` is within this map. We consider a
rune to be dangerous if it has zero optical width:

```go
var defaultGremlins = map[rune]string{
    '\u200b': "zero width space",
    '\u200e': "left-to-right mark",
    '\u2029': "paragraph separator",
    '\u202c': "pop directional formatting",
    '\u202d': "left-to-right override",
    '\u202e': "right-to-left override",
    '\u2066': "Left to right",
    '\u2069': "Pop directional",
    '\ufffc': "object replacement character",
}
```

These runes were used as supply chain attack vectors before,
and we consider them to be unwanted in sources for Lexer.
This behavior is customizable, however. You can add, delete, nil or
clear the map after calling `New` as you see fit. Because it is a copy
of our default map a subseqent `New` call uses the defaults again.

Note: `AcceptPrefix` does not use `Next` and thus won't panic on a
dangerous rune. For `AcceptPrefix` to return true, however, such a rune
needs to be in the function's parameter as well. If it returns false, the
`Current` text is not modified, so a lingering dangerous rune might be
detected by a different helper method.

The map is generated from <https://raw.githubusercontent.com/nhoizey/vscode-gremlins/master/package.json> selecting runes marked as level `error`.

## Using `goyacc` with `Lexer`

A well written and short introduction into using yacc to parse sth
can be found at
[GopherCon 2018 - How to Write a Parser in Go](https://about.sourcegraph.com/blog/go/gophercon-2018-how-to-write-a-parser-in-go)
by Sugu Sougoumarane.

To use goyacc, a port of the old unix bottom-up parser generator yacc to
Go, install it like this:

```txt
$ go install golang.org/x/tools/cmd/goyacc@latest
...
go: downloading golang.org/x/tools v0.13.0
...
```

```txt
$ goyacc -?
flag provided but not defined: -?
Usage of goyacc:
  -l    disable line directives
  -o string
        parser output (default "y.go")
  -p string
        name prefix to use in generated code (default "yy")
  -v string
        create parsing tables (default "y.output")
```

Then have a look into the yaccscenario subfolder.

### To Adapt a Lexer for `goyacc`

Call the parser similar to this Go test code using `lexer.ForYacc()`:

```go
//go:generate goyacc -o y_test.go -v "" yacc_test.y
func TestYaccInputFile(t *testing.T) {
  yyErrorVerbose = true
  lex := lexer.New(input, lexWords)
  // nil means to use the yacc adapter's error logging implementation
  result := yyParse(lexer.ForYacc(lex, nil, func(tok lexer.Token, lval *yySymType) {
    t.Logf("%-12s %v", yyTokname(tok.Type-yyPrivate+2), tok.Value)
    lval.val = tok.Value
  }))
  t.Log("yyParse:", result)
  if result != 0 {
    t.Fail()
  }
}
```

It needs to be accompanied by similar snippets in the yacc source preamble
to make `lval.val` available as string and to use `$1`..`$9` vars in
embedded code:

```yacc
%union {
    val string
}

// tokens with values
%token <val> IDENTIFIER INTLITERAL STRINGLITERAL

%%

aliasline :
    IDENTIFIER ASSIGN IDENTIFIER {
        log.Println("alias:", $1, "->", $3)
    }
;
```

### yacc and "go generate"

goyacc wrks nicely with `go generate`, e.g. put a comment like this
to some source file.

```go
//go:generate goyacc -l -o y_test.go -v "" dsl.y
//
// -l means to not generate "//line dsl.y:3" comments, as we found that
// panics and dlv (the Go debugger) breakpoints won't work properly b/c the
// files listed there are apparently unfindable by tooling without full paths
```

### The yacc Scenario Shows

- How to build a struct hierarchy with yacc (an AST so to speak)
- How to set the resulting top level struct on the `YaccAdapter` with `func SetResult`
- Building a symbol table (called `alias` in the DSL for `$ident`s)
- Then switching (using `func Data`) to checking for defined symbols
- Plus quite standard: serializing the AST to JSON
- and generating Go code by inspecting the AST

Hence a not too small compiler from a DSL -> Go. The test function
`func TestYaccInputFile(t *testing.T)` runs this compiler on the
provided sample data. The DSL is some skeleton functionality to feed a
German accounting system with some data that needs to be transformed
in a complicated way.
