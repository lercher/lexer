// This code is generated by "go generate". DO NOT EDIT.

package lexer

var defaultGremlins = map[rune]string{
    '\u200b': "zero width space",
    '\u200e': "left-to-right mark",
    '\u2029': "paragraph separator",
    '\u202c': "pop directional formatting",
    '\u202d': "left-to-right override",
    '\u202e': "right-to-left override",
    '\u2066': "Left to right",
    '\u2069': "Pop directional",
    '\ufffc': "object replacement character",
}