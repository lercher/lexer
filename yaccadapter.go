package lexer

import (
	"fmt"
	"log"
	"strings"
	"unicode/utf8"
)

// YaccAdapter wraps a Lexer with two functions
// to use it as a lexer for goyacc. YYSymType is
// always yySymType genrated by yacc. Use ForYacc
// to create a YaccAdapter like this:
//
//	adapter := lexer.ForYacc(lx, nil, func(tok lexer.Token, lval *yySymType) {
//	  lval.val = tok.Value
//	})
//
// Note: All parameters use yacc naming conventions yySth
// in places where the yacc generated values should be used.
// This needs to be managed if using non-default generated
// prefixes after starting goyacc with "-p SomeOtherPrefix".
type YaccAdapter[YYSymType any] struct {
	lex       *Lexer
	errFn     func(position, yaccerr string)
	tokToLVal func(tok Token, lval *YYSymType)
	yyTokname func(int) string
	yyPrivate int
	yyMaxTok  int
	Result    YYSymType // Result is optionally set in a yacc production by SetResult(...) to exfiltrate the top level parse result
}

// ForYacc creates a new YaccAdapter from a Lexer.
// If errFn is nil, a default implementation logs
// the lexer's Position incl. the last Lexer error if present,
// and the yacc provided error.
//
// As an example yacc parser call, generated with defaults, use:
//
//	yyErrorVerbose = true
//	result := yyParse(lexer.ForYacc(lex, nil, func(tok lexer.Token, lval *yySymType, data *lexerData) {
//	  lval.val = tok.Value
//	}))
func ForYacc[YYSymType any](
	l *Lexer,
	errFn func(position, yaccerr string),
	tokToLVal func(tok Token, lval *YYSymType),
) *YaccAdapter[YYSymType] {
	if tokToLVal == nil {
		panic("YaccLexer must have a non-nil tokToLVal function")
	}
	return &YaccAdapter[YYSymType]{
		lex:       l,
		errFn:     errFn,
		tokToLVal: tokToLVal,
	}
}

// Lex satisfies yacc's interface for a lexer
func (ya *YaccAdapter[YYSymType]) Lex(lval *YYSymType) int {
	if tok, ok := ya.lex.NextToken(); ok {
		if tok.Type == 0 {
			return 0 // meaning EOF
		}
		if ya.yyTokname != nil {
			l, c, _, _, _ := ya.lex.Position()
			log.Printf("[%3d:%2d %*s] %*s", l, c, ya.yyMaxTok, ya.yyTokname(tok.Type-ya.yyPrivate+2), c, tok.Value)
		}
		ya.tokToLVal(tok, lval)
		return tok.Type
	}
	return 0 // meaning EOF
}

// Error satisfies yacc's interface for a lexer
func (ya *YaccAdapter[YYSymType]) Error(s string) {
	line, col, _, tx, err := ya.lex.Position()

	position := ""
	switch {
	case err != nil && ya.lex.SourceFile != "":
		position = fmt.Sprintf("%s:%d:%d@%q:%v", ya.lex.SourceFile, line, col, tx, err)
	case err != nil && ya.lex.SourceFile == "":
		position = fmt.Sprintf("%d:%d@%q:%v", line, col, tx, err)
	case ya.lex.SourceFile != "":
		position = fmt.Sprintf("%s:%d:%d@%q", ya.lex.SourceFile, line, col, tx)
	default:
		position = fmt.Sprintf("%d:%d@%q", line, col, tx)
	}

	if ya.errFn != nil {
		ya.errFn(position, s)
		return
	}
	log.Println("ERROR", position, s)
}

// WithTokenLogging modifies the YaccAdapter so that token names are
// resolved and logged with the yacc generated yyTokname function
// and yyPrivate index offset.
// Logging happens before the token is handed over to the tokToLVal
// adapter function of ForYacc.
func (ya *YaccAdapter[YYSymType]) WithTokenLogging(yyTokname func(int) string, yyPrivate int) *YaccAdapter[YYSymType] {
	ya.yyTokname = yyTokname
	ya.yyPrivate = yyPrivate
	if ya.yyTokname != nil {
		ya.yyMaxTok = 4
		i := 0
		for {
			i++ // starts at 1
			tn := ya.yyTokname(i)
			if strings.HasPrefix(tn, "tok-") {
				break
			}
			ya.yyMaxTok = max(ya.yyMaxTok, utf8.RuneCountInString(tn))
		}
	}
	return ya
}

// SetResult can be used inside a yacc semantic action to
// put the one lval to the LVal field of the
// YaccAdapter[YYSymType] used for lexing via yyParse.
//
//	import "gitlab.com/lercher/lexer" // in *.y header
//
//	main : sth { lexer.SetResult(yylex, yyDollar, 1) }; // in some production using $1
//
// SetResult panics if yylex is not a pointer to a YaccAdapter[YYSymType].
// ForYacc() returns such a pointer.
func SetResult[YYSymType any](yylex any, yyDollar []YYSymType, idx int) {
	switch ya := yylex.(type) {
	case *YaccAdapter[YYSymType]:
		ya.Result = yyDollar[idx]
	default:
		var zeroYA YaccAdapter[YYSymType]
		err := fmt.Errorf("SetLVal expected yylex to be of type %T, got %T", &zeroYA, yylex)
		panic(err)
	}
}

// WithData assotiates data with the wrapped Lexer
// It can be retrieved again by the funcion NN
func WithData[YYSymType, T any](data *T, ya *YaccAdapter[YYSymType]) *YaccAdapter[YYSymType] {
	SetData(ya.lex, data)
	return ya
}

func (ya *YaccAdapter[YYSymType]) lexer() *Lexer {
	return ya.lex
}

// Data reads the data associated with yylex.
// yylex must have a method "lexer() *Lexer" which is
// satisfied for Lexer and YaccAdapter[T] and hence for
// yylex inside yacc semantic actions.
// It panics, if types don't match.
//
// Note: If working with *Lexer anyway use GetData[T] to be a little
// more type safe.
func Data[T any](yylex any) *T {
	l := yylex.(unwraper).lexer()
	return GetData[T](l)
}
