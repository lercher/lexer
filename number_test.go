package lexer_test

import (
	"fmt"
	"testing"
	"unicode"

	"gitlab.com/lercher/lexer"
	"golang.org/x/text/runes"
)

var (
	alpha   = runes.In(unicode.Letter)
	numeric = runes.In(unicode.Digit)
)

func lexNumber(l *lexer.Lexer) lexer.StateFn {
	// Optional leading sign.
	l.Accept("+-")
	// Is it hex?
	digits := "0123456789"
	if l.Accept("0") && l.Accept("xX") {
		digits = "0123456789abcdefABCDEF"
	}
	l.AcceptRun(digits)
	if l.Accept(".") {
		l.AcceptRun(digits)
	}
	if l.Accept("eE") {
		l.Accept("+-")
		l.AcceptRun("0123456789")
	}
	// Is it imaginary?
	l.Accept("i")
	// Next thing mustn't be alphanumeric.
	if alpha.Contains(l.Peek()) || numeric.Contains(l.Peek()) {
		l.Next()
		return l.Errorf("bad number syntax: %q", l.Current())
	}
	l.Emit(11)
	return nil
}

func TestComplexNumber(t *testing.T) {
	have := "17.08488i is an imaginary number"
	l := lexer.New("", have, lexNumber)
	tok, ok := l.NextToken()
	if !ok {
		t.Fatalf("can't lex %q", have)
	}
	want, got := "{11 17.08488i}", fmt.Sprint(tok)
	if want != got {
		t.Errorf("want %v, got %v", want, got)
	}
}

func TestNonHexNumber(t *testing.T) {
	have := "0xDeadBeefers are not hex\nnot at all!"
	l := lexer.New("", have, lexNumber)
	tok, ok := l.NextToken()
	if !ok {
		t.Fatalf("can't lex %q", have)
	}
	want, got := `{-9 [1:13@"0xDeadBeefer^0xDeadBeefer^s are not hex"] bad number syntax: "0xDeadBeefer"}`, fmt.Sprint(tok)
	if want != got {
		t.Errorf("want %v, got %v", want, got)
	}
}

func TestNonNumber(t *testing.T) {
	have := "22fix and foxy"
	l := lexer.New("", have, lexNumber)
	tok, ok := l.NextToken()
	if !ok {
		t.Fatalf("can't lex %q", have)
	}
	want, got := `{-9 [1:4@"22f^22f^ix and foxy"] bad number syntax: "22f"}`, fmt.Sprint(tok)
	if want != got {
		t.Errorf("want %v, got %v", want, got)
	}
}

func TestEmpty(t *testing.T) {
	have := ""
	l := lexer.New("", have, lexNumber)
	tok, ok := l.NextToken()
	if !ok {
		t.Fatalf("can't lex %q", have)
	}
	want, got := `{11 }`, fmt.Sprint(tok)
	if want != got {
		t.Errorf("want %v, got %v", want, got)
	}
}
