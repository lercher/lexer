package lexer_test

import (
	"fmt"
	"strings"
	"testing"
	"unicode"

	"gitlab.com/lercher/lexer"
	"golang.org/x/text/runes"
)

const (
	_ = iota
	TextToken
	StringToken
	IntToken
)

const input = `
My name is "Wüll I äm 4711" // and I am a comment
My number is 12345 and thats it
`

func lexComment(l *lexer.Lexer) lexer.StateFn {
	for {
		switch l.Next() {
		case lexer.EOF:
			l.Ignore()
			return nil

		case '\n':
			l.Ignore()
			return lexText
		}
	}
}

func lexText(l *lexer.Lexer) lexer.StateFn {
	for {
		switch {
		case l.EOF():
			l.EmitTrimmed(TextToken)
			return nil

		case l.Accept("0123456789"):
			l.Backup()
			l.EmitTrimmed(TextToken)
			return lexInt

		case l.AcceptPrefix("\""):
			l.Backup()
			l.EmitTrimmed(TextToken)
			return lexString

		case l.AcceptPrefix("//"):
			l.Backup()
			l.EmitTrimmed(TextToken)
			return lexComment
		}

		l.Next()
	}
}

func lexInt(l *lexer.Lexer) lexer.StateFn {
	l.AcceptRun("0123456789")
	l.Emit(IntToken)
	return lexText
}

func lexString(l *lexer.Lexer) lexer.StateFn {
	l.Next() // "
	for {
		switch l.Next() {
		case lexer.EOF, '\n':
			return l.Errorf("string literal not terminated: %s", l.Current())

		case '"':
			l.Emit(StringToken)
			return lexText
		}
	}
}

func TestLexer(t *testing.T) {
	l := lexer.New("", input, lexText)
	var toks []lexer.Token

	for {
		tok, ok := l.NextToken()
		if !ok {
			t.Log("done")
			break
		}
		t.Logf("%+v", tok)
		toks = append(toks, tok)
	}

	want := `[{1 My name is} {2 "Wüll I äm 4711"} {1 My number is} {3 12345} {1 and thats it}]`
	got := fmt.Sprint(toks)
	if got != want {
		t.Errorf("\nwant %v\ngot  %v\n", want, got)
	}
}

func TestLexer_AcceptPrefixMultibyteUtf8(t *testing.T) {
	l := lexer.New("", "HäHäHäHooChristmas", nil)
	for i := 0; i < 3; i++ {
		if !l.AcceptPrefix("Hä") {
			t.Errorf("#%d. Hä not accepted", i)
		}
	}
	if l.AcceptPrefix("Hä") {
		t.Errorf("extra Hä accepted")
	}
	if !l.AcceptPrefix("Hoo") {
		t.Errorf("Hoo not accepted")
	}
	if l.Current() != "HäHäHäHoo" {
		t.Fail()
	}
	want := "HäHäHäHoo^" + l.Current() + "^Christmas"
	if _, _, _, got, _ := l.Position(); got != want {
		t.Errorf("want position %v, got %v", want, got)
	}
}

func TestData(t *testing.T) {
	l := &lexer.Lexer{}
	type ldt struct {
		val int
	}

	ld9481 := ldt{9481}
	lexer.SetData(l, &ld9481)

	lexer.GetData[ldt](l).val = 24
	if ld9481.val != 24 {
		t.Errorf("want 24, got %v", ld9481.val)
	}

	lexer.Data[ldt](l).val = 8874
	if ld9481.val != 8874 {
		t.Errorf("want 8874, got %v", ld9481.val)
	}
}

func TestAcceptFunc(t *testing.T) {
	alpha := runes.In(unicode.Letter)
	numeric := runes.In(unicode.Digit)

	l := lexer.New("", "äbc012ß...", nil)

	l.AcceptRunFunc(alpha.Contains, numeric.Contains)
	got, want := l.Current(), "äbc012ß"
	if got != want {
		t.Errorf("want %q, got %q", want, got)
	}
}

func TestAcceptPrefix(t *testing.T) {
	l := lexer.New("", "...teßt.ä.", nil)

	if !l.AcceptPrefix("...") {
		t.Fail()
	}
	if !l.AcceptPrefix("teßt") {
		t.Fail()
	}
	if !l.AcceptPrefix(".ä.") {
		t.Fail()
	}

	l.Backup() // undo .ä.
	l.Backup() // undo test
	l.Backup() // undo ...

	if !l.AcceptPrefix("...") {
		t.Fail()
	}
	if l.Current() != "..." {
		t.Fail()
	}

	if !l.AcceptPrefix("teßt") {
		t.Fail()
	}
	if l.Current() != "...teßt" {
		t.Fail()
	}

	if !l.AcceptPrefix(".") {
		t.Fail()
	}
	if l.Current() != "...teßt." {
		t.Fail()
	}
}

func TestBackupEOF(t *testing.T) {
	l := lexer.New("", "01", nil)

	if l.Next() != '0' {
		t.Fail()
	}
	if l.Next() != '1' {
		t.Fail()
	}
	if l.Next() != lexer.EOF {
		t.Fail()
	}
	if l.Next() != lexer.EOF {
		t.Fail()
	}
	l.Backup() // undo EOF
	l.Backup() // undo EOF
	l.Backup() // undo 1
	l.Backup() // undo 0
	if l.Next() != '0' {
		t.Fail()
	}
	if l.Next() != '1' {
		t.Fail()
	}
}

func TestLexer_AcceptUntilEOF(t *testing.T) {
	l := lexer.New("", `just some text`, nil)
	l.AcceptUntil(`*`) // note that * appears nowhere
	got, want := l.Current(), `just some text`
	if got != want {
		t.Errorf("want [%v], got [%v]", want, got)
	}
}

func TestLexer_AcceptUntilFuncEOF(t *testing.T) {
	l := lexer.New("", `just some text`, nil)
	l.AcceptUntilFunc(func(r rune) bool { return false }) // note the func literal never returns true
	got, want := l.Current(), `just some text`
	if got != want {
		t.Errorf("want [%v], got [%v]", want, got)
	}
}

func TestLexer_AcceptUntil(t *testing.T) {
	l := lexer.New("", `"I'm a \"string\", baby" said the slice of runes.`, nil)

	if !l.Accept(`"`) {
		t.Fail()
	}

	// need to continue if we look at a `\"` so a loop
	for {
		l.AcceptUntil(`"`)
		l.Next() // "

		// -> "I'm a \" | "I'm a \"string\" | "I'm a \"string\", baby"
		t.Log(l.Current())

		if !strings.HasSuffix(l.Current(), `\"`) {
			break
		}
	}

	if !l.AcceptPrefix(" said") {
		t.Fail()
	}
	l.Backup() // undo " said"

	got, want := l.Current(), `"I'm a \"string\", baby"`
	if got != want {
		t.Errorf("want [%v], got [%v]", want, got)
	}
}

func TestLexer_AcceptUntilFunc(t *testing.T) {
	alpha := runes.In(unicode.Letter)
	numeric := runes.In(unicode.Digit)

	l := lexer.New("", "., ; -äbc012ß", nil)

	l.AcceptUntilFunc(alpha.Contains, numeric.Contains)
	got, want := l.Current(), "., ; -"
	if got != want {
		t.Errorf("want %q, got %q", want, got)
	}
}

func TestLexer_Next_Dangerous(t *testing.T) {
	l := lexer.New("dangerous rune", "a\u200ebc", nil)
	defer func() {
		if r := recover(); r != nil {
			err := r.(error)
			got, want := err.Error(), `dangerous rune "left-to-right mark" at byte pos0 1`
			if got != want {
				t.Errorf("want %q, got %q", want, got)
			}
		}
	}()
	_ = l.Next()
	r := l.Next()
	t.Errorf("Next did not panic, returns %v instead", r)
}

func TestLexer_PeekAll(t *testing.T) {
	l := lexer.New("", `just some text`, nil)
	l.AcceptUntil(` `) // without the space
	got, want := l.PeekAll(), ` some text`
	if got != want {
		t.Errorf("want [%v], got [%v]", want, got)
	}
}

func TestLexer_AcceptPrefix(t *testing.T) {
	l := lexer.New("", `just some text`, nil)
	l.AcceptPrefix(`just `)
	got, want := l.Current(), `just `
	if got != want {
		t.Errorf("want [%v], got [%v]", want, got)
	}
}

func TestLexer_DontAcceptPrefix(t *testing.T) {
	l := lexer.New("", `just some text`, nil)
	l.AcceptPrefix(`JUST `)
	got, want := l.Current(), ``
	if got != want {
		t.Errorf("want [%v], got [%v]", want, got)
	}
}

func TestLexer_AcceptPrefixFold(t *testing.T) {
	l := lexer.New("", `JUST SOME TEXT`, nil)
	l.AcceptPrefixFold(`JuSt `)
	got, want := l.Current(), `JUST `
	if got != want {
		t.Errorf("want [%v], got [%v]", want, got)
	}
}
