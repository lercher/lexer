package lexer_test

import (
	"testing"

	"gitlab.com/lercher/lexer"
)

func TestDataOnYaccAdapter(t *testing.T) {
	lx := &lexer.Lexer{}
	ad := lexer.WithData(&lexerData{}, lexer.ForYacc(lx, nil, func(tok lexer.Token, lval *yySymType) {
		lval.val = tok.Value
	}))

	ldad0 := lexer.Data[lexerData](ad)
	ldlx1 := lexer.Data[lexerData](lx)
	ldlx2 := lexer.GetData[lexerData](lx)

	if ldad0 == nil {
		t.Errorf("lexer data of adapter is nil")
	}
	if ldlx1 == nil {
		t.Errorf("lexer data of lexer is nil")
	}
	if ldlx2 == nil {
		t.Errorf("lexer getdata of lexer is nil")
	}

	if ldad0 != ldlx1 {
		t.Errorf("ptr 0/1 unequal")
	}
	if ldlx1 != ldlx2 {
		t.Errorf("ptr 1/2 unequal")
	}

	if ldad0.allSymbolsDeclared {
		t.Fatalf("should be false initially")
	}
	ldad0.allSymbolsDeclared = true

	ldad3 := lexer.Data[lexerData](ad)
	if !ldad3.allSymbolsDeclared {
		t.Fatalf("should be true after change")
	}
}
