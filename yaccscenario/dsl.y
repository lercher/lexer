%{
package lexer_test

import "gitlab.com/lercher/lexer"
%}

%start main

%union {
    bu *BU // end result
    val, val2 string
    alias *Alias
    aliases []Alias
    amount *Amount
    hp, hp2 *Half
    hps []Half
    split *Split
    splits []Split
    nk *NewNumber
    nks []NewNumber
    f *Fall
    fs []Fall
}

// punctuation
%token L_PAREN R_PAREN COLON_EQUALS SLASH L_BRACE R_BRACE L_BRACKET R_BRACKET R_ARROW
// keywords
%token RECHNUNG_KW ZAHLUNG_KW ALIAS_KW CANCEL_BY_KW SWAP_KW INVERT_KW SPLIT_KW SOLL_KW HABEN_KW SUMME_KW EACH_KW BELEG_KW OP_KW NEW_KW AN_KW WAEHRUNG_KW
// tokens with values
%token<val> DOLLAR_IDENTIFIER IDENTIFIER INTEGER_LITERAL STRING_LITERAL

%type<bu>        bu
%type<aliases>   aliaslines aliasblock
%type<alias>     aliasline
%type<fs>        cases
%type<f>         case casebody caseblock caserechung casezahlung
%type<splits>    splitsections
%type<split>     splitsection
%type<hps>       postingtails postings optionaleachsection sumblock splitblock optionalsplit
%type<hp>        postingtail  posting
%type<amount>    amount
%type<nks>       news
%type<nk>        new
%type<val>       figure text optionalwaehrung optionalbeleg optionalop sollhaben cancelby casefilter


%%

main : bu { lexer.SetResult(yylex, yyDollar, 1) }
;

bu : 
    aliasblock
    { lexer.Data[lexerData](yylex).allSymbolsDeclared = true }
    cases
    { 
        $$ = &BU{
            Alias: $1, 
            Faelle: $3,
        } 
    }
;

/* -------------------------------------------------------- alias */

aliasblock : 
    ALIAS_KW L_PAREN aliaslines R_PAREN {$$ = $3}
;

aliaslines : 
    aliaslines aliasline    {$$ = append($$, *$2)}
    | aliasline             {$$ = append($$, *$1)}
;

aliasline :
    DOLLAR_IDENTIFIER COLON_EQUALS IDENTIFIER {
        $$ = &Alias{$1, $3}
    }
;

/* -------------------------------------------------------- Rechnung/Zahlung */

cases :
    /* empty */   { $$ = nil }
    | cases case  { $$ = append($$, *$2) }
;

case :
    caserechung
    | casezahlung
;

caserechung :
    RECHNUNG_KW caseblock { 
        $$ = $2
        $$.TypRZ = "Rechnung" 
    }
;

casezahlung :
    ZAHLUNG_KW caseblock { 
        $$ = $2
        $$.TypRZ = "Zahlung" 
    }
;

caseblock :
    casefilter L_BRACE casebody R_BRACE {
        $$ = $3
        $$.Fallnummer = $<val>1
        $$.Bezeichung = $<val2>1
    }
;

casefilter:
    INTEGER_LITERAL SLASH IDENTIFIER {
        $<val>$, $<val2>$ = $1, $3
    }
;

casebody :
    cancelby news splitsections optionaleachsection {
        $$ = &Fall{
            Stornieren: $1,
            Nummernkreise: $2,
            SplitBuchungen: $3,
            SHBuchungen: $4,
        }
    }
;

cancelby : 
    /* empty */              { $$ = "Swap"}
    | CANCEL_BY_KW INVERT_KW { $$ = "Invert"}
    | CANCEL_BY_KW SWAP_KW   { $$ = "Swap"}
;

news :
    /* empty */ {$$ = nil}
    | news new  {$$ = append($$, *$2)}
;

new :
    NEW_KW IDENTIFIER R_ARROW DOLLAR_IDENTIFIER {
        $$ = &NewNumber{$2, $4}
    }
;

/* -------------------------------------------------------- split/sum */

splitsections :
    /* empty */ {$$ = nil}
    | splitsections splitsection { $$ = append($$, *$2)}
;

splitsection :
    optionalsplit sumblock {
        $$ = &Split{
            Split:  $1,
            Summen: $2,
        }
    }
;

optionalsplit :
    /* empty */  { $$ = nil }
    | splitblock
;

splitblock :
    SPLIT_KW sollhaben L_PAREN postingtails R_PAREN {
        SH, list := $2, $4
        for i := range list {
            list[i].SH = SH
        }
        $$ = list
    }
;

sumblock :
    SUMME_KW L_PAREN postings R_PAREN { $$ = $3 }
    |
    SUMME_KW sollhaben L_PAREN postingtails R_PAREN { 
        SH, list := $2, $4
        for i := range list {
            list[i].SH = SH
        }
        $$ = list
    }
;

/* -------------------------------------------------------- each */

optionaleachsection :
    /* empty */                        { $$ = nil }
    | EACH_KW L_PAREN postings R_PAREN { $$ = $3 }
;

/* -------------------------------------------------------- lines within each */

postings :
    postings posting {$$ = append($$, *$<hp>2, *$<hp2>2)}
    | posting        {$$ = append($$, *$<hp>1, *$<hp2>1)}
;

posting :
    DOLLAR_IDENTIFIER AN_KW postingtail {
        hpS, hpH := *$3, *$3
        hpS.SH, hpH.SH = "S", "H"
        hpS.Konto = $1
        $<hp>$, $<hp2>$ = &hpS, &hpH
    }
;

sollhaben :
    SOLL_KW    {$$ = "S"}
    | HABEN_KW {$$ = "H"}
;

/* -------------------------------------------------------- lines within split */

postingtails :
    postingtails postingtail {$$ = append($$, *$2)}
    | postingtail            {$$ = append($$, *$1)}
;

postingtail:
    DOLLAR_IDENTIFIER amount text optionalop optionalbeleg optionalwaehrung {
        $$ = &Half{
            Konto:    $1,
            Betrag:   $2,
            Text:     $3,
            OP:       $4,
            Beleg:    $5,
            Waehrung: $6,
        }
    }
;

/* -------------------------------------------------------- line properties */

optionalop :
    /* empty */  {$$ = ""}
    | OP_KW text {$$ = $2}
;

optionalbeleg :
    /* empty */     {$$ = ""}
    | BELEG_KW text {$$ = $2}
;

optionalwaehrung :
    /* empty */          {$$ = ""}
    | WAEHRUNG_KW figure {$$ = $2}
;

text : 
    DOLLAR_IDENTIFIER
    | STRING_LITERAL
;

amount :
    L_BRACKET figure R_BRACKET { 
        $$ = &Amount{N: $2, B: $2, S: "0", SS: "0"} 
    }
    | L_BRACKET figure figure figure figure R_BRACKET { 
        $$ = &Amount{N: $2, B: $3, S: $4, SS: $5} 
    }
;

figure :
    DOLLAR_IDENTIFIER
    | INTEGER_LITERAL /* but only 0 */
;
