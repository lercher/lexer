package lexer_test

import (
	"bytes"
	_ "embed"
	"encoding/json"
	"fmt"
	"go/format"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"text/template"

	"gitlab.com/lercher/lexer"
)

//go:embed dsl_input.txt
var dslinput string

// -l means to not generate "//line dsl.y:3" comments, as we found that
// panics and dlv (the Go debugger) breakpoints won't work properly b/c the
// files listed there are apparently unfindable by tooling without full paths
//
//go:generate goyacc -l -o y_test.go -v "" dsl.y
func TestYaccInputFile(t *testing.T) {
	wd, _ := os.Getwd()
	lx := lexer.New(filepath.Join(wd, "dsl_input.txt"), dslinput, lexWords)
	lexer.SetData(lx, &lexerData{symbolTable: make(map[string]bool)})

	yyErrorVerbose = true
	adapter := lexer.ForYacc(lx, nil, func(tok lexer.Token, lval *yySymType) {
		lval.val = tok.Value
	})
	result := yyParse(adapter)
	if result != 0 {
		t.Fatal("yyParse:", result)
	}
	parseResult := adapter.Result.bu
	_, parseResult.Source = filepath.Split(lx.SourceFile)

	f, err := os.Create("dsl_input.json")
	if err != nil {
		t.Fatal(err)
	}
	defer f.Close()

	enc := json.NewEncoder(f)
	enc.SetIndent("", "  ")

	err = enc.Encode(parseResult)
	if err != nil {
		t.Fatalf("%v: %v", f.Name(), err)
	}

	bum := &BUWithMap{BU: *parseResult}
	tpl, err := template.New("").Funcs(template.FuncMap{
		"root": func() any {
			return bum
		},
	}).ParseFiles("dsl_togo.txt")
	if err != nil {
		t.Fatal(err)
	}
	buf := new(bytes.Buffer)
	err = tpl.ExecuteTemplate(buf, "main", bum)
	if err != nil {
		t.Fatal(err)
	}
	gen, err := format.Source(buf.Bytes())
	err = os.WriteFile("dsl_test.go.txt", gen, 0644)
	if err != nil {
		t.Fatal(err)
	}
}

// lexer function follow

var keywords = map[string]int{
	"RECHNUNG":   RECHNUNG_KW,
	"ZAHLUNG":    ZAHLUNG_KW,
	"ALIAS":      ALIAS_KW,
	"SWAP":       SWAP_KW,
	"INVERT":     INVERT_KW,
	"SPLIT":      SPLIT_KW,
	"SOLL":       SOLL_KW,
	"HABEN":      HABEN_KW,
	"SUM":        SUMME_KW,
	"EACH":       EACH_KW,
	"NEW":        NEW_KW,
	"AN":         AN_KW,
	"CANCEL-BY:": CANCEL_BY_KW,
	"BELEG:":     BELEG_KW,
	"OP:":        OP_KW,
	"WÄHRUNG:":   WAEHRUNG_KW,
	"S":          SOLL_KW, // alternate spellings start here
	"H":          HABEN_KW,
	"WAEHRUNG:":  WAEHRUNG_KW,
	"SUMME":      SUMME_KW,
}

var punctuation = map[string]int{
	"(":  L_PAREN,
	")":  R_PAREN,
	":=": COLON_EQUALS,
	"/":  SLASH,
	"{":  L_BRACE,
	"}":  R_BRACE,
	"[":  L_BRACKET,
	"]":  R_BRACKET,
	"->": R_ARROW,
}

func lexWords(l *lexer.Lexer) lexer.StateFn {
	const (
		special     = ":-"
		letters     = "abcdefghijklmnopqrstuvwxyzäöüABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ"
		digits      = "01234567890"
		nonstarters = "ß"
	)

	switch {
	case l.Accept("$"):
		l.AcceptRun(letters + digits)
		alias := l.Current()
		err := lexer.GetData[lexerData](l).symbol(alias)
		if err != nil {
			return l.Errorf("%v", err)
		}
		l.Emit(DOLLAR_IDENTIFIER)
		return lexWords

	case l.Accept(letters):
		l.AcceptRun(letters + digits + nonstarters + special)

		word := l.Current()
		for k, v := range keywords {
			if strings.EqualFold(k, word) {
				l.Emit(v)
				return lexWords
			}
		}

		// is it a proper identifier?
		if strings.ContainsAny(word, special) {
			return l.Errorf("malformed identifier: %q", word)
		}

		l.Emit(IDENTIFIER)
		return lexWords

	case l.EOF():
		return nil

	case l.AcceptRun(" \n\t\r") != 0:
		l.Ignore()
		return lexWords

	case l.Accept(digits):
		return lexIntegerLiteral

	case l.AcceptPrefix(`"`):
		return lexStringLiteral

	case l.AcceptPrefix("//"):
		return lexLineEndComment
	}

	for k, v := range punctuation {
		if l.AcceptPrefix(k) {
			l.Emit(v)
			return lexWords
		}
	}

	return l.Errorf("unrecognized word: %q", l.Current())
}

func lexIntegerLiteral(l *lexer.Lexer) lexer.StateFn {
	l.AcceptRun("0123456789")
	cur := l.Current()
	if cur != "0" && strings.HasPrefix(cur, "0") {
		return l.Errorf("malformed integer literal: %q", cur)
	}
	l.Emit(INTEGER_LITERAL)
	return lexWords
}

func lexStringLiteral(l *lexer.Lexer) lexer.StateFn {
	for {
		switch l.Next() {
		default:
			continue

		case '"':
			l.Emit(STRING_LITERAL)
			return lexWords

		case lexer.EOF, '\r', '\n':
			return l.Errorf("string literal not terminated: %s", l.Current())
		}
	}
}

func lexLineEndComment(l *lexer.Lexer) lexer.StateFn {
	for {
		switch l.Next() {
		default:
			continue

		case lexer.EOF:
			l.Ignore()
			return nil

		case '\n':
			l.Ignore()
			return lexWords
		}
	}
}

type lexerData struct {
	allSymbolsDeclared bool
	symbolTable        map[string]bool
}

func (ld lexerData) symbol(alias string) error {
	if ld.allSymbolsDeclared {
		if ld.symbolTable[alias] {
			return nil // known alias
		}
		return fmt.Errorf("undeclared alias: %s", alias)
	}
	if ld.symbolTable[alias] {
		return fmt.Errorf("alias can be declared only once: %s", alias)
	}
	ld.symbolTable[alias] = true
	return nil
}

type Amount struct {
	N, B, S, SS string
}

type Half struct {
	SH                        string
	Konto                     string
	Betrag                    *Amount
	Text, OP, Beleg, Waehrung string
}

type Split struct {
	Summen []Half
	Split  []Half
}

type Alias struct {
	Alias, Feld string
}

type NewNumber struct {
	Nummernkreis, InAlias string
}

type Fall struct {
	TypRZ          string
	Fallnummer     string
	Bezeichung     string
	Stornieren     string
	Nummernkreise  []NewNumber
	SplitBuchungen []Split
	SHBuchungen    []Half
}

type BU struct {
	Source string
	Alias  []Alias
	Faelle []Fall
}

type BUWithMap struct {
	BU
	alias2field map[string]string
}

func (bum BUWithMap) FieldOf(prefix, a string) string {
	if bum.alias2field == nil {
		bum.alias2field = make(map[string]string, len(bum.Alias))
		for i := range bum.Alias {
			bum.alias2field[bum.Alias[i].Alias] = bum.Alias[i].Feld
		}
	}
	if f, ok := bum.alias2field[a]; ok {
		return prefix + f
	}
	return a
}

// for the generated code ----------------------------------

type Sink struct {
}
type Group struct {
}
type Buchung struct {
}
