package lexer_test

import (
	_ "embed"
	"testing"

	"gitlab.com/lercher/lexer"
)

func lexStaticValid(l *lexer.Lexer) lexer.StateFn {
	l.Emit(ALIAS_KW)
	l.Emit(L_PAREN)
	l.EmitText(DOLLAR_IDENTIFIER, "$this")
	l.Emit(COLON_EQUALS)
	l.EmitText(IDENTIFIER, "ForThat")
	l.Emit(R_PAREN)
	return nil
}

func lexStaticInvalid(l *lexer.Lexer) lexer.StateFn {
	l.Emit(ALIAS_KW)
	l.Emit(L_PAREN)
	l.EmitText(IDENTIFIER, "ForThat")
	l.Emit(COLON_EQUALS)
	l.EmitText(DOLLAR_IDENTIFIER, "$this")
	l.Emit(R_PAREN)
	return nil
}

func lexStaticLexerError(l *lexer.Lexer) lexer.StateFn {
	l.Emit(ALIAS_KW)
	l.Emit(L_PAREN)
	return l.Errorf("$this is an error$")
}

func TestYaccStaticValid(t *testing.T) {
	yyErrorVerbose = true
	lex := lexer.New("", "no-input", lexStaticValid)
	lexer.SetData(lex, &lexerData{symbolTable: make(map[string]bool)})
	result := yyParse(lexer.ForYacc(lex, nil, func(tok lexer.Token, lval *yySymType) {
		t.Log(tok)
		lval.val = tok.Value
	}).WithTokenLogging(yyTokname, yyPrivate))
	t.Log("yyParse:", result)
	if result != 0 {
		t.Fail()
	}
}

func TestYaccStaticInvalid(t *testing.T) {
	errCalled := false
	yyErrorVerbose = true
	lex := lexer.New("", "no-input", lexStaticInvalid)
	result := yyParse(lexer.ForYacc(lex, func(position, yaccerr string) {
		t.Log("expected error message:", position, yaccerr)
		errCalled = true
		if yaccerr != "syntax error: unexpected IDENTIFIER, expecting DOLLAR_IDENTIFIER" {
			t.Errorf("wanted a syntax error, got %q", yaccerr)
		}
	}, func(tok lexer.Token, lval *yySymType) {
		t.Log(tok)
		lval.val = tok.Value
	}))
	t.Log("yyParse:", result)
	if result == 0 {
		t.Errorf("wanted result != 0, got 0")
	}
	if !errCalled {
		t.Errorf("wanted errFn called, was not called")
	}
}

func TestYaccStaticError(t *testing.T) {
	errCalled := false
	yyErrorVerbose = true
	lex := lexer.New("", "no-input", lexStaticLexerError)
	result := yyParse(lexer.ForYacc(lex, func(position, yaccerr string) {
		t.Log("expected error message:", position, yaccerr)
		errCalled = true
		if yaccerr != "syntax error: unexpected $end, expecting DOLLAR_IDENTIFIER" {
			t.Errorf("wanted a lexer error, got %q", yaccerr)
		}
	}, func(tok lexer.Token, lval *yySymType) {
		t.Log(tok)
		lval.val = tok.Value
	}))
	t.Log("yyParse:", result)
	if result == 0 {
		t.Errorf("wanted result != 0, got 0")
	}
	if !errCalled {
		t.Errorf("wanted errFn called, was not called")
	}
}
