package lexer

// Token is streamed by Lexer.Next() if a StateFn
// decides to Emit a token classified by its Type (int).
//
// Value is the full part of the input-string the Lexer
// detected as a token. A client, usually a parser (see e.g. go-yacc)
// normally converts this input to some particular type,
// probably according to the current parsing context.
//
// Please note that we intentionally use int for the Token Type
// b/c a lot of parser generators do the same and it simplifies
// client provided function signatures for StateFn functions by
// not passing around a particular generics type for it.
type Token struct {
	Type  int
	Value string
}
